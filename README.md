![GRIMER](https://raw.githubusercontent.com/pirovc/grimer/main/grimer/img/logo.png)

GRIMER performs analysis of microbiome studies and generates a portable and interactive dashboard integrating annotation, taxonomy and metadata with focus on contamination detection.

## Source-code

https://github.com/pirovc/grimer

## Installation and user manual

https://pirovc.github.io/grimer/

